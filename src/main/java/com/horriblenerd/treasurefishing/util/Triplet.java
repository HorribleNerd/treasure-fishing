package com.horriblenerd.treasurefishing.util;

public class Triplet<A, B, C> {
    private A a;
    private B b;
    private C c;

    public Triplet(A aIn, B bIn, C cIn) {
        this.a = aIn;
        this.b = bIn;
        this.c = cIn;
    }

    public static <A, B, C> Triplet<A, B, C> of(final A first, final B second, final C third) {
        return new Triplet<>(first, second, third);
    }

    public A getFirst() {
        return this.a;
    }

    public B getSecond() {
        return this.b;
    }

    public C getThird() {
        return this.c;
    }
}