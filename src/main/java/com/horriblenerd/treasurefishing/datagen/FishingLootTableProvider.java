package com.horriblenerd.treasurefishing.datagen;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import net.minecraft.advancements.criterion.LocationPredicate;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.DirectoryCache;
import net.minecraft.data.IDataProvider;
import net.minecraft.data.LootTableProvider;
import net.minecraft.loot.*;
import net.minecraft.loot.conditions.ILootCondition;
import net.minecraft.loot.conditions.LocationCheck;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.biome.Biome;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

public abstract class FishingLootTableProvider extends LootTableProvider {

    private static final Logger LOGGER = LogManager.getLogger();
    private static final Gson GSON = new GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create();

    // Filled by subclasses
    protected final Map<ResourceLocation, LootTable.Builder> lootTables = new HashMap<>();

    private final DataGenerator generator;

    public FishingLootTableProvider(DataGenerator dataGeneratorIn) {
        super(dataGeneratorIn);
        this.generator = dataGeneratorIn;
    }

    // Subclasses can override this to fill the 'lootTables' map.
    protected abstract void addTables();

    protected LootTable.Builder createTableTable(String name, LootTableEntry... entries) {
        LootPool.Builder builder = LootPool.builder();
        builder.name(name);
        builder.rolls(ConstantRange.of(1));

        for (LootTableEntry entry : entries) {
            StandaloneLootEntry.Builder<?> entryBuilder;
            if (entry instanceof LootTableEntry.TableEntry) {
                LootTableEntry.TableEntry tableEntry = (LootTableEntry.TableEntry) entry;
                entryBuilder = TableLootEntry.builder(entry.getLocation());
                Biome[] biomeList = tableEntry.getBiomeList();
                if (biomeList.length > 0) {
                    ILootCondition.IBuilder origin;

                    if (biomeList.length == 1)
                        origin = LocationCheck.builder(LocationPredicate.Builder.func_226870_a_().biome(biomeList[0]));
                    else {
                        // Yes, this is needed. I don't know why, it shouldn't matter, but it does. Just don't touch it.
                        origin = LocationCheck.builder(LocationPredicate.Builder.func_226870_a_().biome(biomeList[0])).alternative(LocationCheck.builder(LocationPredicate.Builder.func_226870_a_().biome(biomeList[1])));

                        for (int i = 2; i < biomeList.length; i++)
                            origin.alternative(LocationCheck.builder(LocationPredicate.Builder.func_226870_a_().biome(biomeList[i])));
                    }
                    entryBuilder.acceptCondition(origin);
                }
                builder.addEntry(entryBuilder.weight(entry.getWeight()).quality(entry.getQuality()));
            }
        }
        return LootTable.builder().addLootPool(builder);
    }

    protected LootTable.Builder createFishingTable(String name, LootTableEntry... entries) {
        LootPool.Builder builder = LootPool.builder();
        builder.name(name);
        builder.rolls(ConstantRange.of(1));

        for (LootTableEntry entry : entries) {
            if (entry instanceof LootTableEntry.ItemEntry) {
                LootTableEntry.ItemEntry itemEntry = (LootTableEntry.ItemEntry) entry;
                StandaloneLootEntry.Builder<?> entryBuilder = ItemLootEntry.builder(itemEntry.getItem()).weight(itemEntry.getWeight()).quality(itemEntry.getQuality());
                for (LootFunction.Builder<?> func : itemEntry.getFunctionList()) {
                    entryBuilder.acceptFunction(func);
                }
                builder.addEntry(entryBuilder);
            }
        }
        return LootTable.builder().addLootPool(builder);
    }

    @Override
    // Entry point
    public void act(DirectoryCache cache) {
        addTables();

        Map<ResourceLocation, LootTable> tables = new HashMap<>();
        for (Map.Entry<ResourceLocation, LootTable.Builder> entry : lootTables.entrySet()) {
            tables.put(entry.getKey(), entry.getValue().setParameterSet(LootParameterSets.FISHING).build());
        }
        writeTables(cache, tables);
    }

    // Actually write out the tables in the output folder
    private void writeTables(DirectoryCache cache, Map<ResourceLocation, LootTable> tables) {
        Path outputFolder = this.generator.getOutputFolder();
        tables.forEach((key, lootTable) -> {
            Path path = outputFolder.resolve("data/" + key.getNamespace() + "/loot_tables/" + key.getPath() + ".json");
            try {
                IDataProvider.save(GSON, cache, LootTableManager.toJson(lootTable), path);
            } catch (IOException e) {
                LOGGER.error("Couldn't write loot table {}", path, e);
            }
        });
    }

    @Override
    public String getName() {
        return "HorribleTweaks LootTables";
    }
}
