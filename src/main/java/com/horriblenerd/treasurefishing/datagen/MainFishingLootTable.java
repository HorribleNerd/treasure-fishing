package com.horriblenerd.treasurefishing.datagen;

import com.horriblenerd.treasurefishing.TreasureFishing;
import com.mojang.datafixers.util.Pair;
import net.minecraft.data.DataGenerator;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.Biomes;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainFishingLootTable extends FishingLootTableProvider {

    public MainFishingLootTable(DataGenerator dataGeneratorIn) {
        super(dataGeneratorIn);
    }

    private static final ArrayList<LootTableEntry> ENTRYLIST = new ArrayList<>();

    public static LootTableEntry.TableEntry GENERAL_FISH = new LootTableEntry.TableEntry(TreasureFishing.MODID, "gameplay/fishing/general_fish", 85, -1);
    public static LootTableEntry.TableEntry GENERAL_JUNK = new LootTableEntry.TableEntry(TreasureFishing.MODID, "gameplay/fishing/general_junk", 10, -2);
    public static LootTableEntry.TableEntry GENERAL_TREASURE = new LootTableEntry.TableEntry(TreasureFishing.MODID, "gameplay/fishing/general_treasure", 5, 2);

    public static LootTableEntry.TableEntry OCEAN = new LootTableEntry.TableEntry(TreasureFishing.MODID, "gameplay/fishing/ocean", Biomes.BEACH, Biomes.OCEAN, Biomes.LUKEWARM_OCEAN, Biomes.WARM_OCEAN, Biomes.DEEP_OCEAN, Biomes.DEEP_LUKEWARM_OCEAN, Biomes.DEEP_WARM_OCEAN);
    public static LootTableEntry.TableEntry COLD_OCEAN = new LootTableEntry.TableEntry(TreasureFishing.MODID, "gameplay/fishing/cold_ocean", Biomes.COLD_OCEAN, Biomes.DEEP_COLD_OCEAN);

    public void addTables() {
//        ArrayList<Pair<ResourceLocation, List<Biome>>> entryList = new ArrayList<>();
//
//        entryList.add(getEntry("gameplay/fishing/cold_ocean", Biomes.COLD_OCEAN, Biomes.DEEP_COLD_OCEAN));
//        entryList.add(getEntry("gameplay/fishing/ocean", Biomes.BEACH, Biomes.OCEAN, Biomes.LUKEWARM_OCEAN, Biomes.WARM_OCEAN, Biomes.DEEP_OCEAN, Biomes.DEEP_LUKEWARM_OCEAN, Biomes.DEEP_WARM_OCEAN));
//
//        entryList.add(getEntry("gameplay/fishing/general_fish"));
//        entryList.add(getEntry("gameplay/fishing/general_junk"));
//        entryList.add(getEntry("gameplay/fishing/general_treasure"));

        register(GENERAL_FISH);
        register(GENERAL_JUNK);
        register(GENERAL_TREASURE);

//        register(OCEAN);
//        register(COLD_OCEAN);

        lootTables.put(new ResourceLocation("minecraft", "gameplay/fishing"), createTableTable("fishing", ENTRYLIST.toArray(new LootTableEntry[0])));
    }

    private Pair<ResourceLocation, List<Biome>> getEntry(String name, Biome... biomes) {
        return Pair.of(new ResourceLocation(TreasureFishing.MODID, name), Arrays.asList(biomes));
    }

    public void register(LootTableEntry entry) {
        ENTRYLIST.add(entry);
    }

}
