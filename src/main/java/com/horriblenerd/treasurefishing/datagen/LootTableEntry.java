package com.horriblenerd.treasurefishing.datagen;

import net.minecraft.item.Item;
import net.minecraft.loot.LootFunction;
import net.minecraft.loot.functions.ILootFunction;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.biome.Biome;

import java.util.Objects;

public abstract class LootTableEntry {

    private final String name;
    private final String namespace;

    private final int weight;
    private final int quality;

    public LootTableEntry(String namespace, String name) {
        this.namespace = namespace;
        this.name = name;
        this.weight = 1;
        this.quality = 0;
    }

    public LootTableEntry(String namespace, String name, int weight) {
        this.namespace = namespace;
        this.name = name;
        this.weight = weight;
        this.quality = 0;
    }

    public LootTableEntry(String namespace, String name, int weight, int quality) {
        this.namespace = namespace;
        this.name = name;
        this.weight = weight;
        this.quality = quality;
    }

    public LootTableEntry getEntry() {
        return this;
    }

    public ResourceLocation getLocation() {
        return new ResourceLocation(this.namespace, this.name);
    }

    public String getName() {
        return name;
    }

    public String getNamespace() {
        return namespace;
    }

    public int getWeight() {
        return weight;
    }

    public int getQuality() {
        return quality;
    }

    public static class TableEntry extends LootTableEntry {

        private final Biome[] biomeList;

        public TableEntry(String namespace, String name, Biome... biomes) {
            super(namespace, name);
            this.biomeList = biomes;
        }

        public TableEntry(String namespace, String name, int weight, Biome... biomes) {
            super(namespace, name, weight);
            this.biomeList = biomes;
        }

        public TableEntry(String namespace, String name, int weight, int quality, Biome... biomes) {
            super(namespace, name, weight, quality);
            this.biomeList = biomes;
        }

        public Biome[] getBiomeList() {
            return biomeList;
        }
    }

    public static class ItemEntry extends LootTableEntry {

        private final LootFunction.Builder<?>[] functionList;
        private final Item item;

        public ItemEntry(Item item, LootFunction.Builder<?>... functionList) {
            super(Objects.requireNonNull(item.getRegistryName()).getNamespace(), item.getRegistryName().getPath());
            this.functionList = functionList;
            this.item = item;
        }

        public ItemEntry(Item item, int weight, LootFunction.Builder<?>... functionList) {
            super(Objects.requireNonNull(item.getRegistryName()).getNamespace(), item.getRegistryName().getPath(), weight);
            this.functionList = functionList;
            this.item = item;
        }

        public ItemEntry(Item item, int weight, int quality, LootFunction.Builder<?>... functionList) {
            super(Objects.requireNonNull(item.getRegistryName()).getNamespace(), item.getRegistryName().getPath(), weight, quality);
            this.functionList = functionList;
            this.item = item;
        }

        public Item getItem() {
            return item;
        }

        public LootFunction.Builder<?>[] getFunctionList() {
            return functionList;
        }
    }

}
