package com.horriblenerd.treasurefishing.datagen;

import com.horriblenerd.treasurefishing.TreasureFishing;
import com.horriblenerd.treasurefishing.util.Triplet;
import net.minecraft.data.DataGenerator;
import net.minecraft.item.Item;
import net.minecraft.item.Items;
import net.minecraft.loot.ConstantRange;
import net.minecraft.loot.LootTable;
import net.minecraft.loot.RandomValueRange;
import net.minecraft.loot.functions.*;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.potion.Potions;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.Util;
import net.minecraft.world.biome.Biomes;
import net.minecraft.world.gen.feature.structure.Structure;
import net.minecraft.world.storage.MapDecoration;

import java.util.ArrayList;

public class BiomeLootTables extends FishingLootTableProvider {

    public BiomeLootTables(DataGenerator dataGenerator) {
        super(dataGenerator);
    }

    @Override
    protected void addTables() {
        lootTables.put(new ResourceLocation(TreasureFishing.MODID, "gameplay/fishing/general_fish"), getGeneralFish());
        lootTables.put(new ResourceLocation(TreasureFishing.MODID, "gameplay/fishing/general_junk"), getGeneralJunk());
        lootTables.put(new ResourceLocation(TreasureFishing.MODID, "gameplay/fishing/general_treasure"), getGeneralTreasure());

//        lootTables.put(new ResourceLocation(TreasureFishing.MODID, "gameplay/fishing/ocean"), getOceanTable());
//        lootTables.put(new ResourceLocation(TreasureFishing.MODID, "gameplay/fishing/cold_ocean"), getColdOceanTable());
    }

    private LootTable.Builder getGeneralFish() {
        ArrayList<LootTableEntry.ItemEntry> itemList = new ArrayList<>();

        itemList.add(new LootTableEntry.ItemEntry(Items.COD, 60));
        itemList.add(new LootTableEntry.ItemEntry(Items.SALMON, 25));
        itemList.add(new LootTableEntry.ItemEntry(Items.TROPICAL_FISH, 2));
        itemList.add(new LootTableEntry.ItemEntry(Items.PUFFERFISH, 13));

        return createFishingTable("general_fish", itemList.toArray(new LootTableEntry[0]));
    }

    private LootTable.Builder getGeneralJunk() {
        ArrayList<LootTableEntry.ItemEntry> itemList = new ArrayList<>();

        itemList.add(new LootTableEntry.ItemEntry(Items.LEATHER_BOOTS, 10, -2, SetDamage.func_215931_a(new RandomValueRange(0.0F, 0.9F))));
        itemList.add(new LootTableEntry.ItemEntry(Items.POTION, 10, -2, SetNBT.builder(Util.make(new CompoundNBT(), (nbt) -> nbt.putString("Potion", "minecraft:water")))));
        itemList.add(new LootTableEntry.ItemEntry(Items.BONE, 10, -2));
        itemList.add(new LootTableEntry.ItemEntry(Items.STRING, 5, -2));
        itemList.add(new LootTableEntry.ItemEntry(Items.ROTTEN_FLESH, 10, -2));
        itemList.add(new LootTableEntry.ItemEntry(Items.CLAY_BALL, 10, -2));

        return createFishingTable("general_junk", itemList.toArray(new LootTableEntry[0]));
    }

    private LootTable.Builder getGeneralTreasure() {
        ArrayList<LootTableEntry.ItemEntry> itemList = new ArrayList<>();

        itemList.add(new LootTableEntry.ItemEntry(Items.TRIDENT, 1, 2, SetDamage.func_215931_a(new RandomValueRange(0.0F, 0.9F)), EnchantWithLevels.func_215895_a(ConstantRange.of(30)).func_216059_e()));
        itemList.add(new LootTableEntry.ItemEntry(Items.NAME_TAG, 1, 1));
        itemList.add(new LootTableEntry.ItemEntry(Items.NAUTILUS_SHELL, 1, 2));
        itemList.add(new LootTableEntry.ItemEntry(Items.COMPASS, 1, 1));
        itemList.add(new LootTableEntry.ItemEntry(Items.MAP, 1, 1, ExplorationMap.func_215903_b().func_237427_a_(Structure.field_236380_p_).func_216064_a(MapDecoration.Type.RED_X).func_216062_a((byte)1).func_216063_a(false)));
        itemList.add(new LootTableEntry.ItemEntry(Items.ANVIL, 1, 1));
        itemList.add(new LootTableEntry.ItemEntry(Items.CHIPPED_ANVIL, 1, 1));
        itemList.add(new LootTableEntry.ItemEntry(Items.DAMAGED_ANVIL, 1, 1));
        itemList.add(new LootTableEntry.ItemEntry(Items.CHEST, 1, 1));

        return createFishingTable("general_treasure", itemList.toArray(new LootTableEntry[0]));
    }
//
//    private LootTable.Builder getOceanTable() {
//        ArrayList<Triplet<Item, Integer, Integer>> itemList = new ArrayList<>();
//
//        itemList.add(Triplet.of(Items.SPONGE, 10, 2));
//        itemList.add(Triplet.of(Items.TROPICAL_FISH, 10, -1));
//
//        return createFishingTable("ocean", itemList);
//    }
//
//    private LootTable.Builder getColdOceanTable() {
//        ArrayList<Triplet<Item, Integer, Integer>> itemList = new ArrayList<>();
//
//        itemList.add(Triplet.of(Items.ICE, 10, -2));
//        itemList.add(Triplet.of(Items.PACKED_ICE, 2, 2));
//        itemList.add(Triplet.of(Items.BLUE_ICE, 1, 3));
//
//        return createFishingTable("cold_ocean", itemList);
//    }

}
